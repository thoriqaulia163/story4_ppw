from django.shortcuts import render, redirect
from .models import Kegiatan,Peserta
from .forms import FormKegiatan, FormPeserta

# Create your views here.
def cekkegiatan(request):
    kegiatan = Kegiatan.objects.all()
    responses = {'kegiatan': kegiatan}
    return render(request, 'main/cekkegiatan.html', responses)

def addkegiatan(request):
    pesan = ""
    if request.POST:
        form = FormKegiatan(request.POST)
        if form.is_valid():
            form.save()
            pesan = "berhasil disimpan"
        else:
            pesan = "gagal menyimpan"
    
    form = FormKegiatan()
    responses = {
        'form': form,
        'pesan': pesan,
    }
    return render(request, 'main/addkegiatan.html', responses)

def detailkegiatan(request,id):   
    kegiatan = Kegiatan.objects.get(id=id)
    peserta = Peserta.objects.filter(kegiatan_id__nama_kegiatan = kegiatan.nama_kegiatan)
    
    responses = {'kegiatan':kegiatan,'peserta':peserta,'id':id}
    return render(request, 'main/detailkegiatan.html', responses)

def addpeserta(request,id):
    pesan = ""
    if request.POST:
        form = FormPeserta(request.POST)
        if form.is_valid():
            form.save()
            pesan = "berhasil disimpan"
            return redirect('/kegiatan/detail/'+str(id))
        else:
            pesan = "gagal menyimpan"
    
    form = FormPeserta(initial= {'kegiatan_id': id})
    responses = {
        'form': form,
        'pesan': pesan,
        'id' : id,
    }
    return render(request, 'main/addpeserta.html', responses)

def deletekegiatan(request, id):
    kegiatan = Kegiatan.objects.get(id=id) 
    kegiatan.delete()
    return redirect('/kegiatan')