from django.urls import path

from . import views

app_name = 'kegiatan'

urlpatterns = [
    path('', views.cekkegiatan, name='cekkegiatan'),
    path('add/', views.addkegiatan, name='addkegiatan'),
    path('detail/<int:id>', views.detailkegiatan, name='detailkegiatan'),
    path('add-peserta/<int:id>/' ,views.addpeserta, name='addpeserta'),
    path('delete/<int:id>', views.deletekegiatan, name='deletekegiatan'),
]
