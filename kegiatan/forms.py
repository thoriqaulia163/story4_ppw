from django import forms
from django.forms import ModelForm
from .models import Kegiatan,Peserta


class FormKegiatan(ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'

class FormPeserta(ModelForm):
    class Meta:
        model = Peserta
        fields = ['nama_peserta','kegiatan_id']
    error_messages = {
        'required' : 'Please Type'
    }
    nama_peserta = forms.CharField(label='Nama peserta ', max_length=40, required=True)

    