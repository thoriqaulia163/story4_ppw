from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Kegiatan, Peserta
from .views import cekkegiatan, addkegiatan, detailkegiatan, addpeserta, deletekegiatan

# Create your tests here.

# URL TEST
class TestStory6(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            nama_kegiatan = 'test kegiatan',
            keterangan = 'tester aja',
            id=1
        )
        self.peserta_kegiatan = Peserta.objects.create(
            kegiatan_id = self.kegiatan,
            nama_peserta = 'test nama',
            id=1
        )

    # Test URL
    def test_apakah_url_cekkegiatan_ada(self):
        client = Client()
        response = client.get('/kegiatan/')
        self.assertEquals(response.status_code, 200)

    def test_apakah_url_addpeserta_ada(self):
        client = Client()
        response = client.get(f"/kegiatan/add-peserta/{self.kegiatan.id}/")
        self.assertEquals(response.status_code, 200)
    
    def test_apakah_url_addkegiatan_ada(self):
        client = Client()
        response = client.post('/kegiatan/add/', data= {
            'nama_kegiatan' : 'test',
            'keterangan' : 'tester aja'
        })
        self.assertEquals(response.status_code, 200)
        response = client.get('/kegiatan/add')
        self.assertEquals(response.status_code, 301)
    
    def test_apakah_url_detailpeserta_ada(self):
        client = Client()
        response = client.get(f"/kegiatan/detail/{self.kegiatan.id}")
        self.assertEquals(response.status_code, 200)

    def test_apakah_url_deletekegiatan_ada(self):
        client = Client()
        response = client.get(f"/kegiatan/delete/{self.kegiatan.id}")
        self.assertEquals(response.status_code,302)
     
# Test View
    def test_view_cekkegiatan(self):
        url = '/kegiatan/'
        response = Client().get(url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'main/cekkegiatan.html')
        
    def test_view_addkegiatan(self):
        response = self.client.post('/kegiatan/add/', data= {
            'nama_kegiatan' : 'test',
            'keterangan' : 'tester aja'
        })
        url = Client().get('/kegiatan/add')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'main/addkegiatan.html')
        self.assertEquals(Kegiatan.objects.count(), 2)
    
    def test_view_addpeserta(self):
        url = Client().get(f"/kegiatan/add-peserta/{self.kegiatan.id}/")
        self.assertTemplateUsed(url, 'base.html')
        self.assertTemplateUsed(url, 'main/addpeserta.html')
        
        response = self.client.post(f"/kegiatan/add-peserta/{self.kegiatan.id}/",
        data = {
            'nama_peserta' : 'test peserta',
            'kegiatan_id' : self.kegiatan.id
        })
        self.assertEquals(Peserta.objects.count(),2)

    def test_view_deletekegiatan(self):
        url = Client().get(f"/kegiatan/delete/{self.kegiatan.id}")
    
        self.assertEquals(Kegiatan.objects.count(),0)
        
#Model test
    def test_kegiatan(self):
        self.kegiatan = Kegiatan.objects.create(
            nama_kegiatan = 'test kegiatan',
            keterangan = 'tester aja',
            id=10
        )

        kegiatan2 = Kegiatan.objects.get(id=10)
        self.assertLessEqual(kegiatan2.nama_kegiatan, "test kegiatan")
        self.assertEqual(str(kegiatan2), "test kegiatan")
        self.assertLessEqual(kegiatan2.keterangan, "tester aja")

    def test_peserta(self):
        self.peserta_kegiatan = Peserta.objects.create(
            kegiatan_id =  Kegiatan.objects.create(id=10),
            nama_peserta = 'test nama peserta',
            id=20
        )
        nama2 = Peserta.objects.get(id=20)
        self.assertEqual(str(nama2), "test nama peserta")
        self.assertEqual(nama2.nama_peserta,"test nama peserta")
        self.assertEqual(nama2.kegiatan_id, Kegiatan.objects.get(id=10))