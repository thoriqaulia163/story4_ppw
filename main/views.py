from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')
def extra(request):
    return render(request, 'main/extra.html')
def story1(request):
    return render(request, 'main/story1.html')

