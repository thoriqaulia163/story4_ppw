from django.urls import path

from . import views

app_name = 'matkul'

urlpatterns = [
    path('', views.cekmatkul, name='cekmatkul'),
    path('addmatkul/', views.addmatkul, name='addmatkul'),
    path('delete/<int:id>', views.deletematkul, name='delete_matkul'),
    path('detail-matkul/<int:id>', views.detailmatkul, name='detail_matkul')
    
]
