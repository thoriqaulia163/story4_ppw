from django import forms
from django.forms import ModelForm
from .models import Matkul

choice = [  ('Gasal 2019/2020','Gasal 2019/2020'), 
            ('Genap 2019/2020','Genap 2019/2020'),
            ('Gasal 2020/2021','Gasal 2020/2021'), 
            ('Genap 2020/2021','Genap 2020/2021'),
            ('Gasal 2021/2022','Gasal 2021/2022'), 
            ('Genap 2021/2022','Genap 2021/2022'),
            ('Gasal 2021/2022','Gasal 2021/2022'), 
            ('Genap 2021/2022','Genap 2021/2022')   ]

class AddMatkul(ModelForm):


    class Meta:
        model = Matkul
        fields = ['nama_matkul','nama_dosen','jumlah_sks','desc_matkul','ruang_kelas','tahun_ajar']
    error_messages = {
        'required' : 'Please Type'
    }
 
    nama_matkul = forms.CharField(label='Nama Matkul ', max_length=40, required=True)
    nama_dosen = forms.CharField(label='Nama Dosen ', max_length=25, required=True)
    jumlah_sks = forms.IntegerField(label='Jumlah sks ',  min_value=1, max_value=10, required=True)
    desc_matkul = forms.CharField(label='Desc Matkul ', max_length=100)
    ruang_kelas = forms.IntegerField(label='Ruang Kelas ', required=True, min_value=0)
    tahun_ajar = forms.ChoiceField(label='Tahun Ajar ', choices=choice ,required=True, initial = 'Gasal 2019/2020')

