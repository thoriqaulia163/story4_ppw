from django.db import models


class Matkul(models.Model):
    TAHUN_AJAR = (  ('Gasal 2019/2020','Gasal 2019/2020'), 
                    ('Genap 2019/2020','Genap 2019/2020'),
                    ('Gasal 2020/2021','Gasal 2020/2021'), 
                    ('Genap 2020/2021','Genap 2020/2021'),
                    ('Gasal 2021/2022','Gasal 2021/2022'), 
                    ('Genap 2021/2022','Genap 2021/2022'),
                    ('Gasal 2021/2022','Gasal 2021/2022'), 
                    ('Genap 2021/2022','Genap 2021/2022')   )

    nama_matkul = models.CharField(max_length=40)
    nama_dosen = models.CharField(max_length=25)
    jumlah_sks = models.IntegerField()
    desc_matkul = models.CharField(max_length=100)
    ruang_kelas = models.IntegerField()
    tahun_ajar = models.CharField(choices=TAHUN_AJAR,max_length=15 ,default='Gasal 2019/2020', null=True)
    
    def __str__(self):
        return self.nama_matkul

