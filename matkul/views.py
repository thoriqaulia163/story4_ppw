from django.shortcuts import render, redirect
from .models import Matkul
from .forms import AddMatkul

def cekmatkul(request):
    matkul = Matkul.objects.all()
    responses = {'matakuliah': matkul}
    return render(request, 'main/cekmatkul.html', responses)

def addmatkul(request):
    pesan = ""
    if request.POST:
        form = AddMatkul(request.POST)
        if form.is_valid():
            form.save()
            pesan = "Data Berhasil Disimpan"
        else:
            pesan = "Data tidak valid"
    response = {'input_form' : AddMatkul, 'pesan' : pesan,}
    return render(request, 'main/addmatkul.html', response)

def detailmatkul(request, id):
    matkul = Matkul.objects.get(id=id)
    response = {'matkul':matkul,'id':id}
    return render(request, 'main/detailmatkul.html', response)


def deletematkul(request, id):
    matkul= Matkul.objects.get(id=id) 
    matkul.delete()
    return redirect('/matkul')
